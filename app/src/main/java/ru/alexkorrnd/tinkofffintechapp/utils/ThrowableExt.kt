package ru.alexkorrnd.tinkofffintechapp.utils

import android.content.Context
import ru.alexkorrnd.tinkofffintechapp.R
import ru.alexkorrnd.tinkofffintechapp.presentation.base.ResourceProvider

fun Throwable.userMessage(resourceProvider: ResourceProvider): String {
    return resourceProvider.getString(R.string.default_error_message)
}

fun Throwable.userMessage(context: Context): String {
    return context.getString(R.string.default_error_message)
}